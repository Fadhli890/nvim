local spec = {
    "nvim-treesitter/nvim-treesitter",
    config = {
        auto_install = true,
        additional_vim_regex_highlighting = false,
    },
}

return spec
